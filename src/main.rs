use std::error::Error;

use serde::{Deserialize, Serialize};

use colored::*;

#[derive(Serialize, Deserialize, Debug)]
struct Info {
    #[serde(rename = "IsTor")]
    is_tor: bool,
    #[serde(rename = "IP")]
    ip: String,
}

fn main() -> Result<(), Box<dyn Error>> {
    let info: Info = ureq::get("https://check.torproject.org/api/ip")
        .call()?
        .into_json()?;

    if info.is_tor {
        println!(
            "{}",
            "Your public endpoint IP address is Tor exit node."
                .bright_green()
                .bold()
        );
    } else {
        println!(
            "{}",
            "Your public endpoint IP address is not Tor exit node!"
                .red()
                .bold()
        );
    }
    Ok(())
}
